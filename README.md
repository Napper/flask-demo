# Flask Demo

Based on ideas at https://docs.dataplicity.com/docs/control-gpios-using-rest-api

Update October 2021: the page mentioned above has been updated to use Falcon rather than Flask so does not relate to this project. The original page on which this project was based is currently available on the Internet Archive: https://web.archive.org/web/20210124183814/https://docs.dataplicity.com/docs/control-gpios-using-rest-api
