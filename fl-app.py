#!/usr/bin/python

from flask import request, abort
from flask_api import FlaskAPI
import RPi.GPIO as GPIO
import os, signal, time, threading

# Define uses of IO pins
RELAY = {"A": 16, "B": 18, "C": 22}
L3 = 11
PROG = 13

# Configure IO
GPIO.setmode(GPIO.BOARD)
GPIO.setup(RELAY["A"], GPIO.OUT)
GPIO.setup(RELAY["B"], GPIO.OUT)
GPIO.setup(RELAY["C"], GPIO.OUT)
GPIO.setup(L3, GPIO.OUT)
GPIO.setup(PROG, GPIO.OUT)

app = FlaskAPI(__name__)

# Set up the "routes"

@app.route('/', methods=["GET"])
def api_root():
    return {
        "relay_url": request.url + "relay/(A | B | C)/",
        "relay_url_POST": {"state": "(0 | 1)"},
        "l3_url": request.url + "l3/",
        "l3_url_POST": {"state": "(press | hold | release)"},
        "prog_url": request.url + "prog/",
        "prog_url_POST": {"state": "press"},
        "serial_url": request.url + "serial/",
        "serial_url_POST": {"send": "<text string>"},
        "log_url": request.url + "log/(A | B | C)/",
        "shutdown_url": request.url + "shutdown"
    }
  
@app.route('/relay/<relay_id>/', methods=["GET", "POST"])
def api_relay_control(relay_id):
    if relay_id in RELAY:
        if request.method == "POST":
            GPIO.output(RELAY[relay_id], int(request.data.get("state")))
        return {relay_id: GPIO.input(RELAY[relay_id])}
    else:
        abort(404, description="Invalid relay")

@app.route('/l3/', methods=["GET", "POST"])
def api_l3_control():
    if request.method == "POST":
        if request.data.get("state") == "press":
            GPIO.output(L3, 1)
            GPIO.output(L3, 0)
        elif request.data.get("state") == "hold":
            GPIO.output(L3, 1)
        elif request.data.get("state") == "release":
            GPIO.output(L3, 0)
        else:
            abort(404, description="Invalid state")
    return {"l3": GPIO.input(L3)}

def press_prog():
    GPIO.output(PROG, 1)
    time.sleep(1)
    GPIO.output(PROG, 0)

@app.route('/prog/', methods=["GET", "POST"])
def api_prog_control():
    if request.method == "POST":
        if request.data.get("state") == "press":
            th = threading.Thread(target=press_prog)
            th.start()
        else:
            abort(404, description="Invalid state")
    return {"prog": GPIO.input(PROG)}

last_request = ""
@app.route('/serial/', methods=["GET", "POST"])
def api_serial():
    if request.method == "POST":
        global last_request
        last_request = request.data.get("send")
    if last_request == "":
        return {"serial": ""}
    else:
        return {"serial": "Response to " + last_request}

@app.route('/log/<log_id>', methods=["GET"])
def api_log(log_id):
    with open("README.md","r") as f:
        contents = f.read()
    return { log_id: contents }
  
@app.route('/shutdown', methods=["GET"])
def api_shutdown():
    os.kill(os.getpid(), signal.SIGINT)
    return { "success": True, "message": "Server is shutting down..." }

# Run if called at top level  
if __name__ == "__main__":
    app.run()

# Handle shutdown gracefully
def shutdown():
    print("Shutting down ...")
    GPIO.cleanup()

import atexit
atexit.register(shutdown)
